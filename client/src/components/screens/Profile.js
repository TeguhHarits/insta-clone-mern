import React, { useEffect, useState, useContext } from 'react'
import { UserContext } from "../../App"

const Profile = () => {

    const [mypics, setPics] = useState([])
    const { state, dispatch } = useContext(UserContext)
    useEffect(() => {
        fetch('/allpost', {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwt")
            }
        }).then(res => res.json())
            .then(result => {
                setPics(result.posts)
                console.log(result.posts)
            })
    }, [])

    return (
        <div className="container" >
            <div style={{
                display: "flex", justifyContent: "center", margin: "18px 0px"
            }}>
                <img
                    src="https://images.unsplash.com/photo-1542897730-c4c118a0bf9d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=900&q=60"
                    style={{ width: "160px", height: "160px", borderRadius: "50%", objectFit: "cover" }}
                />
                <div style={{ marginLeft: "50px", width: "100%" }}>
                    <h5>{state ? state.name : "loading"}</h5>
                    <div style={{ display: "flex" }}>
                        <h6 style={{
                            flexBasis: "100%"
                        }}><b>78</b> posts</h6>
                        <h6 style={{
                            flexBasis: "100%"
                        }}><b>40 </b>followers</h6>
                        <h6 style={{
                            flexBasis: "100%"
                        }}><b>35</b> following</h6>
                    </div>
                    <h6><b>Aris Teguh</b></h6>
                </div>
            </div>
            <div className="gallery">
                {
                    mypics.map(item => {
                        return (
                            <img key={item._id} className="item" src={item.photo} alt={item.title} />
                        )
                    })
                }
            </div>

        </div >
    )
}

export default Profile
