import React, { useState, useEffect } from 'react'
import M from "materialize-css"
import { useHistory } from 'react-router-dom'

const CreatePost = () => {
    const history = useHistory()
    const [title, setTitle] = useState("")
    const [body, setBody] = useState("")
    const [image, setImage] = useState("")
    const [url, setUrl] = useState("")
     useEffect(() => {
        if (url) {
           fetch("/createdpost", {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + localStorage.getItem("jwt")

                },

                body: JSON.stringify({
                    title,
                    body,
                    pict: url
                })
            }).then(res => res.json())
                .then(data => {
                    console.log("judul " + title +
                        " content  " + body +
                        " url " + url)
                    if (data.error) {
                        M.toast({ html: data.error, classes: "#c62828 red darken-3" })
                    } else {
                        M.toast({ html: "Successfull created post", classes: "#43a047 green darken-1" })
                        history.push('/')
                    }
                }).catch(err => {
                    console.log(err);
                })
        }


    }, [url])

    const postDetails = async () => {
        const data = new FormData();
        data.append("file", image)
        data.append("upload_preset", "insta-clone")
        data.append("cloud_name", "teguhharits")
        await fetch("https://api.cloudinary.com/v1_1/teguhharits/image/upload", {
            method: "POST",
            body: data
        })
            .then(res => res.json())
            .then(data => {
                setUrl(data.url);
            })
            .catch(err => {
                console.log(err);
            })


    }

    return (
        <div className="container">
            <div
                className="card input-filed"
                style={{
                    padding: '35px 25px 60px 35px',
                    maxWidth: '500px',
                    margin: '26px auto'
                }}>
                <input
                    type="text"
                    placeholder="title"
                    onChange={(e) => setTitle(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="body"
                    onChange={(e) => setBody(e.target.value)}
                />

                <div className="file-field input-field">

                    {/* <div class="btn #ef9a9a red darken-1">
                        <span>Upload Image</span>
                        <input type="file" onChange={(e) => setImage(e.target.files[0])} />
                    </div> */}
                    <span className="btn  #ef9a9a red darken-1" style={{ borderRadius: '10%' }}>
                        <i className="material-icons left">cloud_upload</i>
                        Image<input type="file" onChange={(e) => setImage(e.target.files[0])} />
                    </span>
                    <div className="file-path-wrapper">
                        <input
                            className="file-path validate"
                            style={{ border: 'none !important' }}
                            type="text"

                        />
                    </div>
                    <button
                        className="btn waves-effect waves-light #ef9a9a red darken-1"
                        onClick={() => postDetails()}
                    >
                        Submit Post
                </button>
                </div>

            </div>
        </div>
    )
}

export default CreatePost
