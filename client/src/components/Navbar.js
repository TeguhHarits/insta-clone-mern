import React, { useContext } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { UserContext } from '../App'

const Navbar = () => {
    const { state, dispatch } = useContext(UserContext)
    const history = useHistory()
    const renderList = () => {
        if (state) {
            return [
                <li><Link to="/create">Crete</Link></li>,
                <li> <Link to="/profile">Profile</Link></li>,
                <li>
                    <button
                        className="btn waves-effect waves-light #ef9a9a red darken-1"
                        onClick={() => {
                            localStorage.clear()
                            dispatch({ type: "CLEAR" })
                            history.push('/login')

                        }}
                    >
                        Logout
                </button>
                </li>
            ]
        } else {
            return [
                <li><Link to="/login">Signin</Link></li>,
                <li><Link to="/signup">Signup</Link></li>
            ]
        }
    }
    return (
        <div className="container">
            <nav>
                <div className="nav-wrapper">
                    <Link to={state ? "/" : "/login"} className="brand-logo left">Instagram</Link>
                    <ul id="nav-mobile" className="right">
                        {renderList()}
                    </ul>
                </div>
            </nav>
        </div>

    )
}

export default Navbar
